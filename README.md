# Polyglot OpenFaas Functions

This project is a set of Knative [functions](https://docs.gitlab.com/ee/user/project/clusters/serverless/#deploying-functions) using [OpenFaas runtimes](https://docs.gitlab.com/ee/user/project/clusters/serverless/#openfaas-runtimes)

To deploy these functions:

- You must activate the [Kubernetes integration](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html) in GitLab
- Then, you need to install [Knative via GitLab’s Kubernetes integration](https://docs.gitlab.com/ee/user/project/clusters/serverless/#installing-knative-via-gitlabs-kubernetes-integration)
- You can use [your own installation of Knative](https://docs.gitlab.com/ee/user/project/clusters/serverless/#using-an-existing-installation-of-knative)

When the CI/CD pipeline is complete, you can test the functions with `GET` requests:

> - You can retrieve the url of each functions by selecting the `Operations/Serverless` menu

## How to re-create this project

### Initialize the project

First, you have to install the OpenFaaS CLI. See here: https://docs.openfaas.com/cli/install/

You need to pull in the official [OpenFaaS language templates](https://github.com/openfaas/templates) with this command:

```bash
md polyglot-openfaas-functions
cd polyglot-openfaas-functions
faas-cli template pull
```

The OpenFaaS CLI will create a `template` directory with all the needed templates. 

The, add a `.gitignore` file to your project with this content:

```text
.template
```

### Initialize the functions

I want to create a **CSharp** function, a **GoLang** function, a **Java 11** function and a **Ruby** function. Then type the below commands:

```bash
cp -R template/csharp ./hello-csharp
cp -R template/go ./hello-go
cp -R template/java11 ./hello-java11
cp -R template/ruby ./hello-ruby
```

### Update the functions

#### CSharp

Update `hello-csharp/function/FunctionHandler.cs`:

```csharp
using System;
using System.Text;

namespace Function
{
    public class FunctionHandler
    {
        public string Handle(string input) {
            return "👋 Hello From CSharp 😉\n";
        }
    }
}
```

#### GoLang

Update `hello-go/function/handler.go`:

```go
package function

import (
 "fmt"
)

// Handle a serverless request
func Handle(req []byte) string {
 return fmt.Sprintf("🖐️ Hello from GoLang 😍")
}
```

#### Java 11

Update `hello-java11/function/src/main/java/com/openfaas/function/Handler.java`:

```java
package com.openfaas.function;

import com.openfaas.model.IHandler;
import com.openfaas.model.IResponse;
import com.openfaas.model.IRequest;
import com.openfaas.model.Response;

public class Handler implements com.openfaas.model.IHandler {

    public IResponse Handle(IRequest req) {
        Response res = new Response();
	    res.setBody("🖖 Hello World from Java 11 💥");

	    return res;
    }
}
```

#### Ruby

Update `hello-ruby/function/handler.rb`:

```ruby
class Handler
  def run(req)
    return "Hello world from the ✨ Ruby Language 🎉"
  end
end
```

## And now, time to build and deploy

Add a `.gitlab-cy.yml` file to your project:

```yaml
include:
  - template: Serverless.gitlab-ci.yml 

functions:build:
  extends: .serverless:build:functions
  environment: production

deploy:function:
  extends: .serverless:deploy:functions
  environment: production

```

Run the pipeline, wait a little ⏳, that's all.
